<?php

/**
 * @file
 */
/**
 * Implements hook_drush_command().
 */
function itunesapi_drush_command() {

  $items['itunesapi_libraries_download'] = array(
    'description' => 'Download the Itunes api php class from https://github.com/ijanerik/PHP-iTunes-API.',
    'aliases' => array('iadl'),
  );

  return $items;
}
